from alpine:3.19.1 as build

# arg needs to come after base image
arg ver=1.8.0

# why do we need to manage deps ourselves?
run apk add --no-cache git
run git clone https://github.com/litespeedtech/third-party.git
run apk add --no-cache cmake g++ go make patch perl

# needs to be done now or things break
run mkdir third-party/lib64

# command regularly fails on clone, maybe don't clone the whole repo like a dunce
run sed -i -e 's@git clone https://github.com/google/boringssl.git@git init boringssl@' -e 's@git checkout master@git remote add origin https://github.com/google/boringssl.git@' \
  -e 's/git pull/git fetch --depth 1 origin 9fc1c33e9c21439ce5f87855a6591a9324e569fd/' -e 's/git checkout 9fc1c33e9c21439ce5f87855a6591a9324e569fd/git checkout FETCH_HEAD/' third-party/script/build_bssl.sh
run third-party/script/build_bssl.sh
run mv third-party/include openssl
run mkdir third-party/include
run mv openssl third-party/include
run third-party/script/build_bcrypt.sh
workdir third-party
run git submodule update --init src/ip2loc
workdir ..
run apk add --no-cache autoconf automake libtool
run third-party/script/build_ip2loc.sh
run wget https://github.com/litespeedtech/openlitespeed/archive/refs/tags/v${ver}.tar.gz
run tar -xf v${ver}.tar.gz

# because docker args aren't preserved
run mv openlitespeed-${ver} openlitespeed
workdir openlitespeed
run git clone https://github.com/litespeedtech/lsquic.git
workdir lsquic
run git checkout `cat ../LSQUICCOMMIT`
run git submodule update --init --recursive
workdir ..

# this seems counterintuitive but:
# - allows dependencies to be updated more frequently (on each package release)
# - avoids redownloading the source constantly while debugging
run apk add --no-cache brotli-dev brotli-static bsd-compat-headers expat-dev expat-static libaio-dev libcap-dev libmaxminddb-dev libmaxminddb-static libxml2-static linux-headers pcre-dev udns-dev zlib-dev zlib-static

# no ols, we have our own brotli and it's actually named PROPERLY
run sed -i -e 's/-static//g' CMakeLists.txt
run sed -i -e "s/c_nonshared//g" src/CMakeLists.txt

# - lua isn't needed for static files
# - pagespeed's optimizations are ones we should do manually
# - modsecurity belongs in the reverse proxy layer so we don't have to think about it
run cmake -D MOD_LUA=OFF -D MOD_PAGESPEED=OFF -D MOD_SECURITY=OFF .
run sed -i -e "s/PTHREAD_MUTEX_ADAPTIVE_NP/PTHREAD_MUTEX_NORMAL/g" src/lsr/ls_lock.c
run make

# as much as a static build would be nice here, we lose asyncio if we go static for now
from alpine:3.19.1
run apk add --no-cache libaio libmaxminddb udns
copy --from=build /openlitespeed/src/openlitespeed /bin
copy lsws /usr/local/lsws
cmd openlitespeed -d

